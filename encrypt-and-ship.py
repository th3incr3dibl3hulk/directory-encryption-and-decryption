import os
import subprocess
import sys

def run_command(command):
    """Run a shell command and handle errors."""
    try:
        result = subprocess.run(command, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        print(result.stdout.decode())
        return True
    except subprocess.CalledProcessError as e:
        print(f"Error executing command: {command}\n{e.stderr.decode()}", file=sys.stderr)
        return False

def main():
    directory_to_compress = input("Which directory would you like to encrypt? ")

    if not os.path.isdir(directory_to_compress):
        print(f"Error: The directory '{directory_to_compress}' does not exist.", file=sys.stderr)
        return

    print("Starting compression...")
    compress_action = f"tar czf {directory_to_compress}.tar.gz {directory_to_compress}"
    directory_to_encrypt = f"{directory_to_compress}.tar.gz"

    if run_command(compress_action):
        print("Compression complete.")
    else:
        print("Compression failed.", file=sys.stderr)
        return

    print("Starting encryption...")
    encrypt_action = f"gpg -e -r hau.larry@gmail.com {directory_to_encrypt}"

    if run_command(encrypt_action):
        print("Encryption complete.")
    else:
        print("Encryption failed.", file=sys.stderr)
        return

    final_file = f"{directory_to_encrypt}.gpg"
    print(f"\nFinal output file is: {final_file}")

    print("Starting upload to iDrive Cloud...")
    ship_to_idrive = f"aws s3 cp {final_file} s3://media-offsite --endpoint-url https://h5h8.ch.idrivee2-48.com/"

    if run_command(ship_to_idrive):
        print("\nShipped to iDrive Cloud")

        print("Starting cleanup...")
        cleanup = f"rm -rf {directory_to_encrypt} {final_file}"

        if run_command(cleanup):
            print("Cleanup complete.")
        else:
            print("Cleanup failed.", file=sys.stderr)
    else:
        print("Uploading to iDrive Cloud failed.", file=sys.stderr)

if __name__ == "__main__":
    main()

