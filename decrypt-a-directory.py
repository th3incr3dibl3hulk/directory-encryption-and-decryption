#This script uses GPG encryption.  Be sure you have a GPG key setup locally for execution.
#In this case, it's linked to my email input below.


import os

print("Be sure to run this script in a directory without the decrypted file already present.")
directory_to_decrypt = input("Which directory would you like to decrypt? ")
directory_to_decompress = os.path.splitext(directory_to_decrypt)[0]
print(directory_to_decompress)
new_file = os.path.splitext(directory_to_decompress)[0]
print(new_file)
newer_file = os.path.splitext(new_file)[0]
print(newer_file)


decrypt_action = ("gpg -o {0} -d {1}").format(newer_file, directory_to_decrypt)
os.system(decrypt_action)
print("Decryption complete.")


decompress_action = "tar xzf {0}".format(newer_file)
os.system(decompress_action)
print("Decryption complete.")

print("Cleaning up")
cleanup = ("rm -rf {0}").format(directory_to_decrypt)
os.system(cleanup)
cleanup2 = ("rm -rf {0}").format(newer_file)
os.system(cleanup2)
print("Clean up complete.")
print()
print()

#os.rename(newer_file,newer_file.replace('encrypted-',''))

#print("Final output file is: " + newer_file)

#encrypt
#tar czf encrypted-dir.tar.gz test_directory/
#gpg -e -r hau.larry@gmail.com encrypted-dir.tar.gz


#decrypt
#gpg -o this-other-dir.tar.gz -d encrypted-dir.tar.gz.gpg
#tar xzf this-other-dir.tar.gz
