import subprocess
import sys

def run_command(command):
    """Run a shell command and handle errors."""
    try:
        result = subprocess.run(command, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        print(result.stdout.decode())
        return True
    except subprocess.CalledProcessError as e:
        print(f"Error executing command: {command}\n{e.stderr.decode()}", file=sys.stderr)
        return False

def main():
    folder_to_download = input("Which folder would you like to download from iDrive? ")
    
    print("Starting folder download from iDrive Cloud...")
    # Using recursive flag to download entire folder
    download_command = f"aws s3 cp s3://{folder_to_download} ./{folder_to_download} --recursive --endpoint-url https://h5h8.ch.idrivee2-48.com/"

    if run_command(download_command):
        print("\nFolder download complete!")
    else:
        print("Folder download failed.", file=sys.stderr)

if __name__ == "__main__":
    main() 