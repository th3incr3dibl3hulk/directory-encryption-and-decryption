import subprocess
import sys

def run_command(command):
    """Run a shell command and handle errors."""
    try:
        result = subprocess.run(command, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        print(result.stdout.decode())
        return True
    except subprocess.CalledProcessError as e:
        print(f"Error executing command: {command}\n{e.stderr.decode()}", file=sys.stderr)
        return False

def main():
    file_to_download = input("Which file would you like to download from iDrive? ")
    
    print("Starting download from iDrive Cloud...")
    download_command = f"aws s3 cp s3://media-offsite/{file_to_download} ./ --endpoint-url https://h5h8.ch.idrivee2-48.com/"

    if run_command(download_command):
        print("\nDownload complete!")
    else:
        print("Download failed.", file=sys.stderr)

if __name__ == "__main__":
    main() 