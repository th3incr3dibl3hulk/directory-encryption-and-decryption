#This script uses GPG encryption.  Be sure you have a GPG key setup locally for execution.
#In this case, it's linked to my email input below.


import os

directory_to_compress = input("Which directory would you like to encrypt? ")

compress_action = "tar czf encrypted-{0}.tar.gz {0}".format(directory_to_compress)
directory_to_encrypt = "encrypted-{0}.tar.gz".format(directory_to_compress)
os.system(compress_action)

print("Compression complete.")

encrypt_action = ("gpg -e -r hau.larry@gmail.com {0}").format(directory_to_encrypt)
os.system(encrypt_action)

print("Encryption complete.")

print("Cleaning up")
cleanup = ("rm -rf {0}").format(directory_to_encrypt)
os.system(cleanup)
print("Clean up complete.")


final_file = "{0}.gpg".format(directory_to_encrypt)
print()
print()
print("Final output file is: " + final_file)

#encrypt
#tar czf encrypted-dir.tar.gz test_directory/
#gpg -e -r hau.larry@gmail.com encrypted-dir.tar.gz


#decrypt
#gpg -o this-other-dir.tar.gz -d encrypted-dir.tar.gz.gpg
#tar xzf this-other-dir.tar.gz
